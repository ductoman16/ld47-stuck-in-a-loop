# Ludum Dare 47 - Stuck in a Loop

## Stuck in Orbit _[working title]_
You are a rogue planet, traveling through space, and have to collect various moons to dismantle for resources along your journey

### Gameplay
- ✅ Horizontal sidescrolling
- ✅ Move up and down
- ✅ Bring moons into orbit by moving close to (but not hitting) them
- ✅ Collect 3(?) moons in of a particular type (resource) to dismantle it and collect the resources
- ✅ Can only have 7(same as number of different types of resources?) moons orbiting at any one time

#### Obstacles
- ✅ Time - resources slowly drain over time
- ✅ Moon Collisions - colliding with a moon will drain all resources, except the one the moon is made of (you get a small amount of that one, may be handy in an emergency?)
- ✅ Filling your orbit (not able to match enough up moons) - skim by a black hole to unload unwanted satellites 
- ⬜ Space junk / Asteroids - worthless items that you can collide with or bring into orbit
- ⬜ **(are black holes an obstacle to avoid?)**
- ⬜ **Hostile enemies?** - Bring an alien ship into orbit and it will attack you until you unload it into a black hole
- ⬜ Increase difficulty over time

#### Win condition
- ✅ High scores?

#### Loss condition
- ✅ Run out of any one resource, show game over

#### Start/Restart
- ✅ Start game when you start moving
- ✅ Control instructions on start
- ✅ Restart button when you die

### Music
- ✅ Electronic? Chill? https://www.youtube.com/watch?v=X-2i7Z_AF7w

### Sound Effects
- ✅ Explosion
- ✅ Black hole ambience (based on distance)
- ✅ Chime for matching 3
- 🟨 moon collected noise?

### Graphics 
- ✅ Planet
- ✅ Moons
- ✅ Moon explosion animation
- ✅ Black hole
- ✅ Black hole animation
- ✅ Improve resource bars

#### Optional
- 🟨 Danger low resource indicator
- 🟨 Player died indicator (grayscale planet)
- 🟨 Orbit indicator when near moon (orbit glows, **planet glows?**, or moon glows?)
- ⬜ Moon collected animation
- ⬜ Moon animations
- ⬜ Planet animation




🟦