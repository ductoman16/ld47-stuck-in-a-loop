extends Node

export var player_node_path: NodePath
export var tween_node_path: NodePath
export var resource_info_node_path: NodePath

onready var player = get_node(player_node_path)
onready var tween = get_node(tween_node_path)
onready var resource_info = get_node(resource_info_node_path)

var resource_bar = preload("res://ResourceBar_Vertical.tscn")

func _ready() -> void:
	for index in range(player.resources.size()):
		var key = player.resources.keys()[index]
		create_resource_bar(key)

func create_resource_bar(resource_key: String) -> void:
	var new_bar: Control = resource_bar.instance()
	new_bar.init(resource_key, player.initial_resource_amount, tween, player)
	var texture_progress: TextureProgress = new_bar.get_node("Node2D/TextureProgress")
	#TODO: Let the resource bar texture itself instead of reaching into it
	texture_progress.tint_progress = resource_info.resource_colors[resource_key]
	add_child(new_bar)
