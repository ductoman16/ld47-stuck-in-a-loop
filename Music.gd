extends AudioStreamPlayer

export(Array, AudioStreamSample) var music_files
var random = RandomNumberGenerator.new()

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	random.randomize()
	print("playing " + str(stream.resource_path))
	play()

func _on_Music_finished() -> void:
	var file: Resource = music_files[random.randi_range(0, music_files.size() -1)]
	stream = file
	print("playing " + str(file.resource_path))
	play()
