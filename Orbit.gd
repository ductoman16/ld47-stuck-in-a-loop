extends Node2D

var Moon = load("res://Moon.gd")

export var orbit_distance: int = 70
export var orbit_speed: float = .002
export var max_satellites: int = 7
export var match_number: int = 3

var angle_per_orbit: float = 360 / max_satellites

export var match_sound_path: NodePath
onready var match_sound = get_node(match_sound_path)

signal moons_matched(resource_key)

func _ready() -> void:
	pass # Replace with function body.

func _physics_process(delta: float) -> void:
#	clean_dead_children()
	move_orbiting_children(delta)
	check_for_match()

func clean_dead_children() -> void:
	var children = get_children()
	for child in children:
		if !is_instance_valid(child):
			print("removing invalid child")
			remove_child(child)

func move_orbiting_children(delta: float):
	var children = get_children()
	var time = OS.get_ticks_msec() * orbit_speed

	for index in range(children.size()):
		var child = children[index]
		var offset = ((2*PI) / max_satellites) * index
		child.position = Vector2(orbit_distance * cos(time + offset), orbit_distance * sin(time + offset))
		#https://godotengine.org/qa/50695/rotate-object-around-origin
		#child.position = child.position.rotated(delta)

func add_orbiting_child(node: Node2D) -> void:
	print("adding orbiting child " + node.name)
	if get_child_count() >= max_satellites:
		return

	node.get_parent().remove_child(node)
	self.add_child(node)
	# position correctly

func get_moon_children() -> Array:
	var moon_children = []
	for child in get_children():
		if child is Moon:
			moon_children.append(child)
	return moon_children

func check_for_match() -> void:
	var count = get_resource_count()

	for resource in count:
		if count[resource] >= match_number:
			remove_from_orbit(resource)
			play_match_sound()
			emit_signal("moons_matched", resource)

func play_match_sound() -> void:
	match_sound.play()

func get_resource_count() -> Dictionary:
	var count = {}
	for child in get_moon_children():
		if count.has(child._resource):
			count[child._resource] += 1
		else:
			count[child._resource] = 1
	return count

func remove_from_orbit(resource_key) -> void:
	var removed = 0;
	for child in get_moon_children():
		if child._resource == resource_key:
			remove_child(child)
			removed +=1
			if removed >= match_number:
				return
