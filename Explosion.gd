extends Node2D

func play() -> void:
	get_node("AnimatedSprite").play("explode")

func _on_AnimatedSprite_animation_finished() -> void:
	queue_free()
