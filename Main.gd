extends Node2D

export var player_node: NodePath
export var score_node: NodePath
export var game_over_node: NodePath
export(Array, NodePath) var spawner_nodes: Array
export(Array, NodePath) var title_nodes: Array

onready var score = get_node(score_node)
onready var player = get_node(player_node)
onready var game_over = get_node(game_over_node)

var game_started = false
var game_ended = false

func _ready() -> void:
	player.connect("player_died", self, "_on_player_died")

func _process(delta: float) -> void:
	if !game_started:
		if Input.is_action_pressed("move_up") || Input.is_action_pressed("move_down"):
			start_game()

func start_game() -> void:
	player.is_playing = true
	enable_spawners()
	enable_score()
	hide_title_nodes()
	game_started = true

func hide_title_nodes() -> void:
	for node_path in title_nodes:
		var node: CanvasItem = get_node(node_path)
		node.hide()

func enable_spawners():
	for node_path in spawner_nodes:
		var spawner = get_node(node_path)
		if spawner.has_method("enable"):
			spawner.enable(true)

func enable_score():
	score.running = true

func _on_player_died() -> void:
	game_ended = true
	show_game_over()

func show_game_over():
	game_over.show()
