extends KinematicBody2D

var moon_class = load("res://Moon.gd")

signal player_died
signal resource_changed(resource_key, resource_value)

export var vertical_speed: int = 150
export var vertical_acceleration: float = .05
export var velocity = Vector2(150, 0)

export var initial_resource_amount: int = 200
export var resource_decay: float = .03
export var collision_resource_loss: float = 10
export var collision_resource_gain: float = 20
export var matched_resource_value: int = 70

export var camera_shake_amount: int = 7
export var camera_shake_time: float = .2
var camera_shake_time_remaining: float
onready var camera = get_node("Camera2D")


export var orbit_node_path: NodePath
onready var orbit = get_node(orbit_node_path)

var is_playing = false

var resources = {
	"Nickel": initial_resource_amount,
	"Iron": initial_resource_amount,
	"Cobalt": initial_resource_amount,
	"Magnesium": initial_resource_amount,
	"Water": initial_resource_amount,
	"Vegetation": initial_resource_amount,
}

func _ready() -> void:
	orbit.connect("moons_matched", self, "_on_Orbit_moons_matched")

func _physics_process(delta: float) -> void:
	decrease_resources()
	handle_death()
	handle_movement()
	shake_camera(delta)

func handle_death() -> void:
	if !is_alive():
		is_playing = false
		emit_signal("player_died")

func is_alive() -> bool:
	for resource in resources:
		if resources[resource] <= 0:
			return false
	return true

func decrease_resources() -> void:
	if !is_playing:
		return
	for resource in resources:
		set_resource_value(resource, resources[resource] - resource_decay)

func set_resource_value(resource_key: String, resource_value: float) -> void:
	resources[resource_key] = resource_value
	emit_signal("resource_changed", resource_key, resource_value)

func handle_movement() -> void:
	if is_playing:
		if Input.is_action_pressed("move_up"):
			velocity.y = lerp(velocity.y, -vertical_speed, vertical_acceleration)
		elif Input.is_action_pressed("move_down"):
			velocity.y = lerp(velocity.y, vertical_speed, vertical_acceleration)
		else:
			velocity.y = lerp(velocity.y, 0, vertical_acceleration)
	move_and_slide(velocity,  Vector2.UP)

func _on_Gravity_body_entered(body: Node) -> void:
	print("body entered");

func _on_Gravity_body_exited(body: Node) -> void:
	print("body exited");

	if !body.get("orbiting"):
		if body is moon_class:
			print("It's a moon!")
		if body.has_method("start_orbit"):
			body.start_orbit(orbit)

func _on_Gravity_area_exited(area: Area2D) -> void:
	print("area exited player gravity")

	if area.is_queued_for_deletion():
		print("area is queued for deletion, ignoring")
		return

	if !area.get("orbiting"):
		if area is moon_class:
			print("It's a moon!")
		if area.has_method("start_orbit"):
			area.start_orbit(orbit)

func _on_Orbit_moons_matched(resource_key):
	set_resource_value(resource_key, resources[resource_key] + matched_resource_value)

func collide(body: Node2D) -> void:
	if body is moon_class:
		var resource = body.get_resource()
		adjust_resources_after_collision(resource)

	start_camera_shake()

func adjust_resources_after_collision(resource:String):
	for key in resources:
		if key == resource:
			set_resource_value(key, resources[key] + collision_resource_gain)
		else:
			set_resource_value(key, resources[key] - collision_resource_loss)

func start_camera_shake() -> void:
	camera_shake_time_remaining = camera_shake_time

func shake_camera(delta) -> void:
	if camera_shake_time_remaining > 0:
		camera.set_offset(Vector2( \
			rand_range(-1.0, 1.0) * camera_shake_amount, \
			rand_range(-1.0, 1.0) * camera_shake_amount \
		))
		camera_shake_time_remaining -= delta
