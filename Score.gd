extends RichTextLabel

export var player_node_path: NodePath
onready var player_node = get_node(player_node_path)

var running: bool = false
var score: int = 0

func _ready() -> void:
	player_node.connect("player_died", self, "_on_player_died")

func _process(delta: float) -> void:
	if running:
		score += int(delta * 100)
	text = str(score)

func _on_player_died() -> void:
	running = false
