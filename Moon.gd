extends Area2D

export var velocity = Vector2()

const Explosion = preload("res://Explosion.tscn")
const Orbit = preload("res://Orbit.gd")
var Player = load("res://Player.gd")

var orbiting: bool = false

func is_orbiting() -> bool:
	return orbiting

var _resource: String

signal destroyed

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	if _resource == null:
		print("moon created without resource!")
		return

#	animated_sprite.play(_resource + "_idle")

func start_orbit(orbit: Orbit):
	orbit.add_orbiting_child(self)
	orbiting = true

func set_resource(resource: String, color: Color):
	_resource = resource
	var sprite: Sprite = get_node("Sprite")
	sprite.texture = load("res://images/" + _resource + ".png")

func get_resource() -> String:
	return _resource

func _on_Area2D_body_entered(body: Node) -> void:
	print("moon collided with body")
	if body.has_method("collide"):
		body.collide(self)
		destroy()

func destroy() -> void:
	create_explosion()

	queue_free()
	emit_signal("destroyed")
	print("moon queued for deletion")

func create_explosion() -> void:
	var explosion = Explosion.instance()
	get_parent().add_child(explosion)
	explosion.global_transform = global_transform
	explosion.play()
