extends Node2D

export var spawn_chance: int = 3
export var parent: NodePath;
export var spawn_origin: NodePath;

const BlackHole = preload("res://BlackHole.gd")
var black_hole_scene = load("res://BlackHole.tscn")
var random = RandomNumberGenerator.new()

var screenWidth: int
var screenHeight: int

var spawning_enabled = false

func _ready() -> void:
	screenWidth = get_viewport_rect().size.x
	screenHeight = get_viewport_rect().size.y
	random.randomize()

func _process(delta: float) -> void:
	if spawning_enabled && random.randi_range(0, 1000) < spawn_chance:
		spawn()

func spawn() -> void:
	var black_hole_instance: BlackHole = black_hole_scene.instance()

	get_node(parent).add_child(black_hole_instance)

	black_hole_instance.position = randomize_position()

func randomize_position() -> Vector2:
	var spawn_origin_position = get_node(spawn_origin).position
	var x = spawn_origin_position.x + screenWidth;
	var y = spawn_origin_position.y + random.randi_range(-screenHeight, screenHeight)
	return Vector2(x,y)

func enable(enabled: bool) -> void:
	spawning_enabled = enabled
