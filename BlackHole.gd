extends Node2D

const Moon = preload("res://Moon.gd")
onready var satellites = get_node("Satellites")

func _ready() -> void:
	pass # Replace with function body.

func _physics_process(delta: float) -> void:
	move_children_towards_center(delta)

func _on_Area2D_area_exited(area: Area2D) -> void:
	if area.is_queued_for_deletion():
		return

	if area is Moon:
		print("Moon collided with black hole! ")
		reparent(area)

func reparent(node: Moon) -> void:
	var current_position = node.get_global_position()
	node.get_parent().remove_child(node)
	satellites.add_child(node)
	node.set_global_position(current_position)

func move_children_towards_center(delta: float) -> void:
	# TODO: Fix bug when collecting moon that overlaps a black hole
	for child in satellites.get_children():
		if child == null:
			continue
		child.position = lerp(child.position, Vector2.ZERO, delta)

func _on_Area2D_area_entered(area: Area2D) -> void:
	destroy_moons_spawned_overlapping(area)

func destroy_moons_spawned_overlapping(area: Area2D) -> void:
	if area.is_queued_for_deletion():
		return

	if area.has_method("is_orbiting") && !area.is_orbiting():
		print("deleting overlapping moon")
		area.queue_free()
