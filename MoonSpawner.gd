extends Node2D

export var spawn_chance: int = 45
export var moon_parent: NodePath;
export var moon_spawn_origin: NodePath;

var moon = load("res://Moon_Area2D.tscn")
var random = RandomNumberGenerator.new()

var screenWidth: int
var screenHeight: int

var spawning_enabled = false

var resource_colors = {
	"Nickel": Color(1,1,1),
	"Iron": Color(0.859,0.314,0.290),
	"Cobalt": Color(0.008,0.031,0.529),
	"Magnesium": Color(0.725,0.373,0.537),
	"Water": Color(0.0,0.647,0.812),
	"Vegetation": Color(0.145,0.631,0.557),
}

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	screenWidth = get_viewport_rect().size.x
	screenHeight = get_viewport_rect().size.y
	random.randomize()


func _process(delta: float) -> void:
	if spawning_enabled && random.randi_range(0, 1000) < spawn_chance:
		spawn_moon()

func spawn_moon() -> void:
	var moon_instance: Area2D = moon.instance()
	set_random_resource(moon_instance)

	get_node(moon_parent).add_child(moon_instance)

	moon_instance.position = randomize_moon_position()

func randomize_moon_position() -> Vector2:
	var spawn_origin_position = get_node(moon_spawn_origin).position
	var x = spawn_origin_position.x + screenWidth;
	var y = spawn_origin_position.y + random.randi_range(-screenHeight, screenHeight)
	return Vector2(x,y)

func set_random_resource(moon_instance: Area2D) -> void:
	var resource = resource_colors.keys()[random.randi_range(0, resource_colors.size()-1)]
	moon_instance.set_resource(resource, resource_colors[resource])

func enable(enabled: bool) -> void:
	spawning_enabled = enabled
