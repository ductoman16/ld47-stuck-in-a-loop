extends VBoxContainer

onready var bar = $Node2D/TextureProgress

var tween: Tween
var resource_key: String
var resource_max: int

var animated_health = 100
var tween_duration_seconds:float = 0.2

func init(resource_key: String, resource_max: int, tween: Tween, player: Node) -> void:
	self.resource_key = resource_key
	self.resource_max = resource_max
	self.tween = tween
	print("resource bar initialized for " + resource_key + ". Max value: " + str(resource_max))
	player.connect("resource_changed", self, "_on_Player_resource_changed")

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	bar.max_value = resource_max
	update_resource_amount(resource_max)

func _on_Player_resource_changed(resource_key: String, resource_value: int) -> void:
	if(self.resource_key == resource_key):
		update_resource_amount(resource_value)

func update_resource_amount(new_value: int) -> void:
	tween.interpolate_property(self, "animated_health", animated_health, new_value, tween_duration_seconds, Tween.TRANS_LINEAR, Tween.EASE_IN)
	if not tween.is_active():
		tween.start()

func _process(delta):
	var round_value = round(animated_health)
#	number_label.text = str(round_value)
	bar.value = round_value
