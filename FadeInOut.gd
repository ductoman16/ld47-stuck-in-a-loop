extends CanvasItem

export var tween_node: NodePath

onready var tween: Tween = get_node(tween_node)

var is_hiding: bool = false
var is_showing: bool = false
var alpha: float

func hide() -> void:
	alpha = modulate.a
	is_hiding = true

func show() -> void:
	modulate.a = 0
	visible = true
	is_showing = true

func _process(delta: float) -> void:
	if is_hiding:
		tween.interpolate_property(self, "alpha", alpha, 0, 0.6, Tween.TRANS_LINEAR, Tween.EASE_IN)
		modulate.a = alpha

	if is_showing:
		tween.interpolate_property(self, "alpha", alpha, 1, 0.6, Tween.TRANS_LINEAR, Tween.EASE_IN)
		modulate.a = alpha
